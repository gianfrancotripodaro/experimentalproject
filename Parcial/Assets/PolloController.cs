using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PolloController : MonoBehaviour
{
    public Rigidbody2D rb;

    public Camera camCocina;

    public GameObject sartenConPollo;
    public GameObject sarten;

    public Vector3 position;
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        position = this.transform.position;
    }


    private void OnMouseDrag()
    {
        Vector3 mousePosition = new Vector3(Input.mousePosition.x, Input.mousePosition.y, Input.mousePosition.z + 2.5f);
        Vector3 objPosition = camCocina.ScreenToWorldPoint(mousePosition);
        transform.position = objPosition;
        rb.isKinematic = true;

    }

    private void OnMouseUp()
    {
        rb.isKinematic = false;
    }



    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag=="Sarten")
        {
            Instantiate(sartenConPollo, position, Quaternion.identity);
            Destroy(sarten);
            Destroy(this.gameObject);

        }
    }
}
