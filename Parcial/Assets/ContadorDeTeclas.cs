using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ContadorDeTeclas : MonoBehaviour
{


    public static int contadorTeclas;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    //public void contarTecla()
    //{
    //    if ()
    //    {

    //    }
    //}


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Control") || collision.gameObject.CompareTag("C") || collision.gameObject.CompareTag("V"))
        {
            contadorTeclas++;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Control") || collision.gameObject.CompareTag("C") || collision.gameObject.CompareTag("V"))
        {
            contadorTeclas--;
        }
    }

}
