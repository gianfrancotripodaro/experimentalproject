using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeclaControl : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (PcController.meSaliDeLaPc)
        {
            Destroy(gameObject, 10);
        }
        //Debug.Log(PcController.PorcentajeTrabajado);
    }

    private void OnMouseDown()
    {
        TocarTeclaLogica();
    }

    public void TocarTeclaLogica()
    {
        if (WorkController.prenderApagarPc)
        {


            switch (gameObject.tag)
            {
                case "Control":
                    if (!WorkController.ctrlOn)
                    {
                        WorkController.ctrlOn = true;
                        WorkController.vOn = false;
                        Destroy(gameObject);
                    }
                    break;

                case "C":

                    if (WorkController.ctrlOn && !WorkController.vOn)
                    {
                        WorkController.cOn = true;
                        WorkController.ctrlOn = false;
                        Destroy(gameObject);
                    }

                    break;

                case "V":

                    if (WorkController.ctrlOn && WorkController.cOn)
                    {
                        WorkController.vOn = true;
                        WorkController.ctrlOn = false;
                        WorkController.cOn = false;
                        PcController.PorcentajeTrabajado++;
                        Destroy(gameObject);
                    }

                    break;
            }

        }
        
    }
}
