using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Animations;

public class PolloYSartenController : MonoBehaviour
{
    public Rigidbody2D rb;

    private Camera camCocina;

    public static bool unoPolloYSarten;

    public float cocinandoPolloTimer;

    public GameObject cocido;
    public GameObject quemado;

    public Animator camAnim;

    public static float contAnim;

    //public GameObject camPJ;

    public bool cocinandoPollo;

    public bool listoParaComer;

    public bool terminarANIM;

    //public bool terminoAnim;
    void Start()
    {
        //camCocina = GetComponent<Camera>();

        camAnim = GameObject.Find("CamaraCocina").GetComponent<Animator>();
        camCocina = GameObject.Find("CamaraCocina").GetComponent<Camera>();
        //camPJ = /* GameObject.Find("Personaje").GetComponent<GameObject>();*/

        unoPolloYSarten = true;

    }

    // Update is called once per frame
    void Update()
    {

        if (terminarANIM)
        {
            contAnim += Time.deltaTime;
        }


        if (cocinandoPollo)
        {
            cocinandoPolloTimer += Time.deltaTime;
        }

        if (cocinandoPolloTimer>50 && cocinandoPolloTimer < 100)
        {
            //cocido
            listoParaComer = true;
            cocido.SetActive(true);
        }
        else
        {
            if (cocinandoPolloTimer > 100)
                quemado.SetActive(true);
        }

        //if( cocinandoPolloTimer > 100)
        //  {
        //      //quemado

        //  }
    }


    private void OnMouseDrag()
    {
      
        Vector3 mousePosition = new Vector3(Input.mousePosition.x, Input.mousePosition.y, Input.mousePosition.z + 2.5f);
        Vector3 objPosition = camCocina.ScreenToWorldPoint(mousePosition);
        transform.position = objPosition;
        rb.isKinematic = true;

    }

    private void OnMouseUp()
    {
        rb.isKinematic = false;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "HornoPrendido")
        {
            //cocinandoPollo +=
            cocinandoPollo = true;
        }
    }

  

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "HornoPrendido")
        {
            //cocinandoPollo +=
            cocinandoPollo = false;

            if (listoParaComer)
            {

                PrenderAnim();
            }
        }
    }




    public void PrenderAnim()
    {

        //ejecutar animacion
        //camAnim.Play("");
        camAnim.enabled = true;
        terminarANIM = true;
        //if (contAnim > 23)
        //{

        //    //camPJ.SetActive(true);

        //}

    }

}
