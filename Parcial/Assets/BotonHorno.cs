using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BotonHorno : MonoBehaviour
{
    public GameObject prenderHrono;

    public bool HornoPrendido;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    private void OnMouseDown()
    {
        if (HornoPrendido)
        {
            prenderHrono.SetActive(false);
            HornoPrendido = false;
        }
        else
        {
            prenderHrono.SetActive(true);

            HornoPrendido = true;
        }
    }
}
