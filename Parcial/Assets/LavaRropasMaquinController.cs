using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LavaRropasMaquinController : MonoBehaviour
{
    public GameObject abrirLavarropas;

    public static bool lavaRropasAbierto;

    public int contRopa;

    public GameObject lavarropasLlenoAbierto;
    public static bool estaLavaRropasAbiertoLleno;

    public static bool estalavaRopasLLenoCerrado;

    public GameObject lavaRropasLlenoCerrado;



    //public GameObject lavaRropasAndando;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (contRopa > 2 && !estalavaRopasLLenoCerrado)
        {

            lavaRropasLlenoCerrado.SetActive(false);
            lavarropasLlenoAbierto.SetActive(true);
                abrirLavarropas.SetActive(false);
                estaLavaRropasAbiertoLleno = true;
          
         
        }
        else
        {
            lavarropasLlenoAbierto.SetActive(false);
            estaLavaRropasAbiertoLleno = false;
        }
    }


    private void OnMouseDown()
    {
        if (lavaRropasAbierto)
        {
            abrirLavarropas.SetActive(false);
            lavaRropasAbierto = false;
        }
        else
        {
            //if (!estalavaRopasLLenoCerrado)
            //{
                abrirLavarropas.SetActive(true);

                lavaRropasAbierto = true;
            //}
           
        }


        if (estaLavaRropasAbiertoLleno)
        {
            //lavarropasLlenoAbierto.SetActive(false);
            lavaRropasLlenoCerrado.SetActive(true);
            estalavaRopasLLenoCerrado = true;

        }
        else
        {

            lavaRropasLlenoCerrado.SetActive(false);
            estalavaRopasLLenoCerrado = false;
        }

    }



    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Ropa" && lavaRropasAbierto)
        {
            contRopa++;
        }
    }
}
