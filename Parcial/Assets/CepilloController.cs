using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Animations;

public class CepilloController : MonoBehaviour
{
    public Rigidbody2D rb;

    public Camera camBa�o;

    public SpriteRenderer bocaSucia;

    public float alphaDientes;

    public Color sacarMugre;

    public Animator camAnim;

    public float contAnim;

    public static bool salirBa�o;

    public GameObject dialogoDientes;

    void Start()
    {
        alphaDientes = 1;
        camAnim.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {

        if (alphaDientes<=0)
        {
            AnimDientesLimpios();
        }

        Color colorActual = bocaSucia.color;
        Color nuevoColor = new Color(colorActual.r, colorActual.g, colorActual.b, alphaDientes);
        bocaSucia.color = nuevoColor;

    }


    private void OnMouseDrag()
    {
        Vector3 mousePosition = new Vector3(Input.mousePosition.x , Input.mousePosition.y, Input.mousePosition.z + 2.5f);
        Vector3 objPosition = camBa�o.ScreenToWorldPoint(mousePosition);
        transform.position = objPosition;
        rb.isKinematic = true;

    }

    private void OnMouseUp()
    {
        rb.isKinematic = false;
    }



    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "BocaSucia")
        {
            alphaDientes -= 0.2f;

        }
    }



    public void AnimDientesLimpios()
    {
        //ejecutar animacion
        //camAnim.Play("");
        camAnim.enabled = true;
        contAnim += Time.deltaTime;
        dialogoDientes.SetActive(false);
        if (contAnim > 23)
        {
            salirBa�o = true;
        }
    }


}
