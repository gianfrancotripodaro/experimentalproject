using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    [SerializeField] private float speed = 3f;

    private Rigidbody2D player1Rb;

    private Vector2 moveInput;

    public static bool tocandoPc;

    public GameObject PjCenital;

    public GameObject PjLateral;

    public bool PjCenitalActivado;

    public static bool tocandoCocina;

    public static bool tocandoBa�o;

    public GameObject hambreDialogo;

    public Animator walkAnim;

    public GameObject paredPieza;

    public static bool tocandoLavaRopa;



    void Start()
    {
        player1Rb = GetComponent<Rigidbody2D>();
        //walkAnim = GetComponent<Animator>();

    }

    // Update is called once per frame
    void Update()
    {
        //Debug.Log(PjCenitalActivado);
        //Debug.Log(tocandoPc);
        float moveX = Input.GetAxisRaw("Horizontal");
        float moveY = Input.GetAxisRaw("Vertical");

        walkAnim.SetFloat("Horizontal", moveX);
        walkAnim.SetFloat("Vertical", moveY);
        walkAnim.SetFloat("Speed", moveInput.sqrMagnitude);

        moveInput = new Vector2(moveX, moveY).normalized;


        if (EventosManager.tengoHambre && PolloYSartenController.contAnim < 21)
        {
            hambreDialogo.SetActive(true);
            
        }
        else
        {
            hambreDialogo.SetActive(false);
            
        }

    }

    private void FixedUpdate()
    {
        player1Rb.MovePosition(player1Rb.position + moveInput * speed * Time.fixedDeltaTime);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("PC"))
        {
            tocandoPc = true;
        }

        if (collision.gameObject.CompareTag("PiezaPuerta"))
        {

            if (PjCenitalActivado)
            {
                PjCenital.SetActive(false);
                PjLateral.SetActive(true);
                PjCenitalActivado = false;
            }
            else
            {
                PjCenital.SetActive(true);
                PjLateral.SetActive(false);
                PjCenitalActivado = true;
            }
        }

        if (collision.gameObject.CompareTag("Cocina"))
        {
            tocandoCocina = true;
        }

        if (collision.gameObject.CompareTag("Ba�o"))
        {
            tocandoBa�o = true;
        }

        if (collision.gameObject.CompareTag("LavaRopa"))
        {
            tocandoLavaRopa = true;
        }

    }


    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("PC"))
        {
            tocandoPc = false;
        }

        if (collision.gameObject.CompareTag("Cocina"))
        {
            tocandoCocina = false;
        }

        if (collision.gameObject.CompareTag("Ba�o"))
        {
            tocandoBa�o = false;
        }

        if (collision.gameObject.CompareTag("LavaRopa"))
        {
            tocandoLavaRopa = false;
        }
    }

    //private void OnTriggerExit(Collider other)
    //{
    //    if (other.gameObject.CompareTag("PC"))
    //    {
    //        tocandoPc = false;

    //    }
    //}

}
