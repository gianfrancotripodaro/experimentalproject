using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SalirCocina : MonoBehaviour
{
    public GameObject cocinaCam;

    public GameObject pjCam;

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
      
        if (PolloYSartenController.contAnim > 20)
        {
            pjCam.SetActive(true);
            cocinaCam.gameObject.SetActive(false);
        }
    }



    private void OnMouseDown()
    {
        cocinaCam.SetActive(false);
        pjCam.SetActive(true);
    }
}
