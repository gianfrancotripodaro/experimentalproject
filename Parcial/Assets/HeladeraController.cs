using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeladeraController : MonoBehaviour
{

    public GameObject heladeraAbierta;

    public bool abrirHeladera;

    public GameObject pollo;

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }



    private void OnMouseDown()
    {
        if (abrirHeladera)
        {
            heladeraAbierta.SetActive(false);
            abrirHeladera = false;
        }
        else
        {
            heladeraAbierta.SetActive(true);
            if (!PolloYSartenController.unoPolloYSarten)
            {
                pollo.SetActive(true);
            }
              
            abrirHeladera = true;
        }
       
    }
}
