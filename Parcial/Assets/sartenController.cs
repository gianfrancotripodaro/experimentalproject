using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class sartenController : MonoBehaviour
{
    public Rigidbody2D rb;

    public Camera camCocina;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }



    private void OnMouseDrag()
    {
        Vector3 mousePosition = new Vector3(Input.mousePosition.x, Input.mousePosition.y, Input.mousePosition.z + 2.5f);
        Vector3 objPosition = camCocina.ScreenToWorldPoint(mousePosition);
        transform.position = objPosition;
        rb.isKinematic = true;

    }

    private void OnMouseUp()
    {
        rb.isKinematic = false;
    }
}
