using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RopaController : MonoBehaviour
{
    public Camera camCocina;
    public Rigidbody2D rb;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnMouseDrag()
    {

        Vector3 mousePosition = new Vector3(Input.mousePosition.x, Input.mousePosition.y, Input.mousePosition.z + 2.5f);
        Vector3 objPosition = camCocina.ScreenToWorldPoint(mousePosition);
        transform.position = objPosition;
        rb.isKinematic = true;

    }


    private void OnMouseUp()
    {
        rb.isKinematic = false;
    }


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag=="LavaRopa" && LavaRropasMaquinController.lavaRropasAbierto)
        {
            Destroy(gameObject);
        }
    }
}
