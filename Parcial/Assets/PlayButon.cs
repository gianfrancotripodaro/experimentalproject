using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Animations;
using UnityEngine.SceneManagement;

public class PlayButon : MonoBehaviour
{

    public Animator camAnim;
    public static bool leDioPlay;
    public float contBorrarCam;
    public GameObject CepillarDientesMSJ;

    public Animator AnimTitulo;
    public Animator AnimBotones;
    public Animator AnimCreditosNombre;


    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (leDioPlay)
        {
            contBorrarCam += Time.deltaTime;
            if (contBorrarCam > 31f)
            {
                camAnim.gameObject.SetActive(false);
                CepillarDientesMSJ.SetActive(true);

            }
        }
    }

    //private void OnMouseDown()
    //{
    //    camAnim.enabled = true;
    //    leDioPlay = true;
    //}


    public void ClickBotonPlay()
    {
        camAnim.enabled = true;
        leDioPlay = true;
    }

    public void ClickBotonCredits()
    {
        //camAnim.enabled = true;
        //leDioPlay = true;

        AnimTitulo.enabled = true;
        AnimBotones.enabled = true;
        AnimCreditosNombre.enabled = true;

    }

    public void ClickBotonExit()
    {
        //camAnim.enabled = true;
        //leDioPlay = true;
        Application.Quit();

    }
}
