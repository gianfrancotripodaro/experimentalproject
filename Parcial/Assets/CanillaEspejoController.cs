using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanillaEspejoController : MonoBehaviour
{
    public GameObject BañoCam;

    public GameObject pjCam;

    public GameObject bañoIluminado;

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (CepilloController.salirBaño)
        {
            BañoCam.SetActive(false);
            pjCam.SetActive(true);
        }

        if (PlayerMovement.tocandoBaño)
        {
            bañoIluminado.SetActive(true);
        }
        else
        {
            bañoIluminado.SetActive(false);
        }
    }

    private void OnMouseDown()
    {
        BañoCam.SetActive(true);
        pjCam.SetActive(false);
    }

}
