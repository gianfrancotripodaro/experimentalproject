using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BotonLavarropas : MonoBehaviour
{
    public GameObject tapaGirar;
    public bool girando;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    private void OnMouseDown()
    {
        if (girando)
        {
            tapaGirar.SetActive(false);
            girando = false;
        }
        else
        {
            tapaGirar.SetActive(true);
            girando = true;
        }
       
    }
}
