using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PcController : MonoBehaviour
{
    public GameObject iluminarPC;

    public GameObject camaraPj;

    public GameObject camaraTrabajo;

    public bool Trabajando;

    public float trabajoTotal = 20f;

    public Image fillimage;

    public static float PorcentajeTrabajado;

    public static bool meSaliDeLaPc;

   

    //public float timer = 20f;

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

        //WorkController.prenderApagarPc = true;
        if (Trabajando)
        {
            EventosManager.timerLlamada += Time.deltaTime;
        }
     

        ActivarObjetos();
        //IrEspacioDeTrabajo();


        if (PorcentajeTrabajado == 20)
        {
            Debug.Log("Ganaste");
        }
        else
        {
            //PorcentajeTrabajado++;

            fillimage.fillAmount = PorcentajeTrabajado / trabajoTotal;
        }

    }



    public void ActivarObjetos()
    {
        if (PlayerMovement.tocandoPc)
        {
            iluminarPC.SetActive(true);

            //if (Input.GetKeyDown(KeyCode.Mouse0))
            //{
            //    IrEspacioDeTrabajo();
            //}
            
        }
        else
        {
            iluminarPC.SetActive(false);
        }
    }



    private void OnMouseDown()
    {
        IrEspacioDeTrabajo();
    }


    public void IrEspacioDeTrabajo()
    {
        meSaliDeLaPc = false;
        camaraTrabajo.SetActive(true);
        camaraPj.SetActive(false);
        Trabajando = true;

      

    }


    public void EncendidoApagadoPc()
    {
        if (WorkController.prenderApagarPc)
        {
            //camaraTrabajo.SetActive(false);
            //camaraPj.SetActive(true);
            //meSaliDeLaPc = true;
            WorkController.prenderApagarPc = false;
            camaraTrabajo.SetActive(false);
            camaraPj.SetActive(true);
            meSaliDeLaPc = true;

        }
        else
        {

            WorkController.prenderApagarPc = true;

        }

        

        //WorkController.prenderApagarPc = true;

    }



}
