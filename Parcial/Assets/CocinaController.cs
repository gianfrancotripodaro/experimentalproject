using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CocinaController : MonoBehaviour
{
    public GameObject cocinaIluminada;

    public GameObject cocinaCam;

    public GameObject pjCam;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (PlayerMovement.tocandoCocina)
        {
            cocinaIluminada.SetActive(true);
        }
        else
        {
            cocinaIluminada.SetActive(false);
        }

   

    }



    private void OnMouseDown()
    {
        IrCocinaVista();
    }


    public void IrCocinaVista()
    {
        cocinaCam.SetActive(true);
        pjCam.SetActive(false);
    }


    //public void SalirCocina()
    //{
    //    cocinaCam.SetActive(false);
    //    pjCam.SetActive(true);
    //}
}
