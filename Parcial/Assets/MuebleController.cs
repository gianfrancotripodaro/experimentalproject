using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MuebleController : MonoBehaviour
{
    public GameObject muebleAbierto;
    public GameObject sarten;

    public bool abrirmueble;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    private void OnMouseDown()
    {
        if (abrirmueble)
        {
            muebleAbierto.SetActive(false);
            abrirmueble = false;
        }
        else
        {
            muebleAbierto.SetActive(true);
            if (!PolloYSartenController.unoPolloYSarten)
            {
                sarten.SetActive(true);
            }
            
            abrirmueble = true;
        }
    }
}
