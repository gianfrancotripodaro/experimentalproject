using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorkController : MonoBehaviour
{
    public static bool ctrlOn;

    public static bool vOn;

    public static bool cOn;

   public GameObject controlTecla;
    public GameObject cTecla;
    public GameObject vTecla;
    public static bool prenderApagarPc;

    public GameObject blackScreen;

    public GameObject DesesperadoCara;

    public float contBreak;

    void Start()
    {
        //blackScreen.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {


        if (contBreak > 10)
        {
            DesesperadoCara.SetActive(false);
            //contBreak = 0;
        }

        if (PcController.meSaliDeLaPc)
        {
            contBreak += Time.deltaTime;

         
        }
       

        if (prenderApagarPc)
        {
            blackScreen.SetActive(false);
            //Debug.Log(ContadorDeTeclas.contadorTeclas);
            if (ContadorDeTeclas.contadorTeclas > 60)
            {
                //contBreak += Time.deltaTime;
                SalirPc();
            }
            //else
            //{
            //    DesesperadoCara.SetActive(false);
            //}


            //SpawnTeclas();
        }
        else
        {
            blackScreen.SetActive(true);
        }

        //LogicaTelcas();
    }



    //public void SpawnTeclas()
    //{
       
    //}


    public void GenerarTeclas()
    {
        if (prenderApagarPc)
        {


            float posXGeneracion = Random.Range(28.5f, 31.6f);
            float posYGeneracion = Random.Range(0.8f, -0.6f);
            float teclaAleatoria = Random.Range(1, 5);
            Vector3 posAleatoria = new Vector2(posXGeneracion, posYGeneracion);

            switch (teclaAleatoria)
            {
                case 1:
                    Instantiate(controlTecla, posAleatoria, Quaternion.identity);
                    break;

                case 2:
                    Instantiate(cTecla, posAleatoria, Quaternion.identity);
                    break;

                case 3:
                    Instantiate(vTecla, posAleatoria, Quaternion.identity);
                    break;

                case 4:
                    Instantiate(vTecla, posAleatoria, Quaternion.identity);
                    break;

                case 5:
                    Instantiate(controlTecla, posAleatoria, Quaternion.identity);
                    break;

            }
        }
    }

    public void SalirPc()
    {
        Debug.Log("Me sal� de la pc");

        if (contBreak <10)
        {
            DesesperadoCara.SetActive(true);
        }
       

       

   
        //if ()
        //{

        //}
    }


    //public void LogicaTelcas()
    //{
    //    //if ()
    //    //{

    //    //}
    //}
}
