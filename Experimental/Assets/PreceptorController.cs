using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Animations;

public class PreceptorController : MonoBehaviour
{
   public float AqueHoraVieneElprece;

    public Animator PreceAnim;

    public GameObject MiraPreceptor;

    public int rayDistance = 10;


    public GameObject Colisionador;

    public bool PreceAdentroDelAula;

    public float contAbreLosOjos;

    void Start()
    {
        
    }




    // Update is called once per frame
    void Update()
    {
        AqueHoraVieneElprece += Time.deltaTime;

        EntrarAula();
        SalirAula();
        DetectarMirando();

        //if ()
        //{

        //}
    }


    public void EntrarAula()
    {
        if (AqueHoraVieneElprece > 20 && AqueHoraVieneElprece < 60)
        {
            PreceAnim.Play("PreceEntrar");
            //ruido cantando
        }
    }

    public void SalirAula()
    {
        if (AqueHoraVieneElprece > 60)
        {
            PreceAnim.Play("PreceSalir");
            //ruido se va
        }
    }



    public void DetectarMirando()
    {
        Debug.DrawRay(MiraPreceptor.transform.position, MiraPreceptor.transform.TransformDirection(new Vector3(1, 0, 0)) * rayDistance, Color.green);
        RaycastHit hit3;


        if (Physics.Raycast(MiraPreceptor.transform.position, MiraPreceptor.transform.TransformDirection(new Vector3(1, 0, 0)), out hit3, rayDistance))
        {
            Debug.Log(hit3.transform.tag);

            if (hit3.transform.tag == "Player" && !ChatController.guardado && PreceAdentroDelAula && contAbreLosOjos >3)
            {
                RayCastController.Perdiste = true;
            }

        }

    }


    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Colisionador"))
        {
            PreceAdentroDelAula = true;
            //ruido abrir puerta
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.CompareTag("Colisionador"))
        {
            contAbreLosOjos += Time.deltaTime;
            //ruido abrir puerta
        }
    }


}
