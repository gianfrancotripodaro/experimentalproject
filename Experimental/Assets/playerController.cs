using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerController : MonoBehaviour
{

    public Transform CamaraPlayer;

    public int rayDistance;

    public static bool vistaAlfrente;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        DetectarMirando();


    }



    public void DetectarMirando()
    {
        Debug.DrawRay(CamaraPlayer.transform.position, CamaraPlayer.transform.TransformDirection(new Vector3(1, 0, 0)) * rayDistance, Color.blue);
        RaycastHit hit2;


        

        if (Physics.Raycast(CamaraPlayer.transform.position, CamaraPlayer.transform.TransformDirection(new Vector3(1, 0, 0)), out hit2, rayDistance))
        {
            Debug.Log(hit2.transform.tag);

            if (hit2.transform.tag == "Pizarron")
            {
                vistaAlfrente = true;
            }
            else
            {
                vistaAlfrente = false;
            }
        }

    }
}
