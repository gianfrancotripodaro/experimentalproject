using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Animations;
using UnityEngine.UI;
using TMPro;


public class Personaje1 : MonoBehaviour
{

   public Animator Anim1;

    public float cont;

    public bool RespuestaPasada;

    public GameObject ExamenA1;

    //public GameObject boton;

    public static bool PasandoRespuesta;

    public TMP_InputField RT1;

    public Text TimerPersonaje1;

    void Start()
    {
        Anim1.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {


  


        if (RT1.text != "27346")
        {

            cont += Time.deltaTime;
            int myInt = Mathf.FloorToInt(cont);
            TimerPersonaje1.text = myInt.ToString() + " /20";

            if (cont > 10 && !RespuestaPasada)
            {
                Debug.Log("Pasame la respuesta o le digo a la profe");

            }
            if (cont > 20 && !RespuestaPasada)
            {
                Anim1.enabled = true;
                //RayCastController.Perdiste = true;
            }
            //else
            //{

            //    Anim1.enabled = false;

            //    Debug.Log("Gracias Capo");
            //}


        }
        else
        {
            Anim1.enabled = false;

            Debug.Log("Gracias Capo");
        }


    }



    public void Espera()
    {
        cont = 0;
    }

    public void PasarRespuesta()
    {

        
        if (!PasandoRespuesta)
        {
            PasandoRespuesta = true;
            ExamenA1.transform.position += new Vector3(2f, 0, 0);
            //boton.SetActive(false);
        }
        else
        {
            PasandoRespuesta = false;
            ExamenA1.transform.position -= new Vector3(2f, 0, 0);
            //boton.SetActive(true);
        }
        //ExamenA1.transform.position += new Vector3(2f, 0, 0);
        //boton.SetActive(false);

        RespuestaPasada = true;
    }




    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Perder"))
        {
            Debug.Log("Te cagaron");
            RayCastController.Perdiste = true;
        }
    }

}
