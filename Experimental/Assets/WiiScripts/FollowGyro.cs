using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowGyro : MonoBehaviour
{

    [Header("Tweaks")]
    [SerializeField] private Quaternion baseRotation = new Quaternion(-1, -1, 1, -1);
    private void Start()
    {
        GyroManager.Instance.EnableGyro();
    }

    private void Update()
    {
        transform.localRotation = GyroManager.Instance.GetGyroRotation()  * baseRotation;
    }





}
