using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public static bool DerechaOn;
    public static bool IzquierdaOn;

    public GameObject ButonDerecha;
    public GameObject ButonIzquierda;

    void Start()
    {

        DerechaOn = false;
        IzquierdaOn = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (playerController.vistaAlfrente)
        {
            IzquierdaOn = false;
            DerechaOn = false;
            ButonDerecha.SetActive(true);
            ButonIzquierda.SetActive(true);
        }
    }



    public void Derecha()
    {
        if (DerechaOn == false)
        {
            this.transform.Rotate(new Vector3(0f, 100f, 0f) * -3);
            DerechaOn = true;
            //ButonDerecha.SetActive(false);
        }
        else
        {
            //DerechaOn = false;
            //ButonDerecha.SetActive(true);
        }

        //DerechaOn = true;
        ////if ()
        ////{
        ////    transform.Rotate(new Vector3(0f, 100f, 0f) * 3);
        ////}

        //this.transform.Rotate(new Vector3(0f, 100f, 0f) * -3);


    }

    public void Izquierda()
    {
        if (IzquierdaOn == false)
        {
            this.transform.Rotate(new Vector3(0f, 100f, 0f) * 3);
            IzquierdaOn = true;
            //ButonIzquierda.SetActive(false);
        }
        else
        {
            //ButonIzquierda.SetActive(true);
        }

        
        //if ()
        //{
        //    transform.Rotate(new Vector3(0f, 100f, 0f) * 3);
        //}

        
    }
}
