using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AutoScroll : MonoBehaviour
{

    RectTransform rt;


    private void OnEnable()
    {
        ChatManager.OnMessageRecived += ScrollDown;
    }
    private void OnDisable()
    {
        ChatManager.OnMessageRecived -= ScrollDown;
    }


    void Start()
    {
        rt = GetComponent<RectTransform>();
    }

    private void ScrollDown()
    {
        LayoutRebuilder.ForceRebuildLayoutImmediate(rt);
        StartCoroutine(ForceScrollDown());

    }
    IEnumerator ForceScrollDown()
    {
        yield return new WaitForEndOfFrame();
        Vector2 anchoredPosition = rt.anchoredPosition;
        anchoredPosition.y = Mathf.Max(0, rt.sizeDelta.y);
        rt.anchoredPosition = anchoredPosition;
    }
}
