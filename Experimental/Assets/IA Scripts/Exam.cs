using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;

public class Exam : MonoBehaviour
{
    public static bool guardado;
    public GameObject Examen;
    public GameObject botonAbrir;
    public GameObject botonCerrar;
    public GameObject respuestas;
    public TMP_InputField RT1;
    public TMP_InputField RT2;
    public TMP_InputField RT3;
    public TMP_InputField RT4;
    void Start()
    {
        guardado = true;
        botonCerrar.SetActive(false);
        respuestas.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {

        ExamenRespuestas();



    }

   public void AbrirExamen()
    {
        //Debug.Log("anda");
        if (guardado)
        {
            //desplegarExamen();
            botonAbrir.SetActive(false);
            guardado = false;
            Examen.transform.position += new Vector3(0.15f, 0.35f, -0.1f);
            Examen.transform.Rotate(new Vector3(-90f, 0f, 0f) * 1);
            botonCerrar.SetActive(true);
            respuestas.SetActive(true);
        }
        else
        {
            botonAbrir.SetActive(true);
            guardado = true;
            botonCerrar.SetActive(false);
            respuestas.SetActive(false);
            Examen.transform.position -= new Vector3(0.15f, 0.35f, -0.1f);
            Examen.transform.Rotate(new Vector3(+90f, 0f, 0f) * 1);
        }
    }



   //public void desplegarExamen()
   // {

   //     Examen.transform.position += new Vector3(0.15f, 0.35f, -0.1f);
   //     Examen.transform.Rotate(new Vector3(-90f, 0f, 0f) * 1);


   // }


    public void ExamenRespuestas()
    {

        //GEO
        if (SceneManager.GetSceneByName("LVL1Examen") == SceneManager.GetActiveScene())
        {
            if (RT1.text == "Monte Logan" || RT1.text == "Logan" || RT1.text == "monte logan")
            {
                RT1.textComponent.color = Color.green;
                Debug.Log("BIEN");
            }
            else
            {
                RT1.textComponent.color = Color.red;
            }

            if (RT2.text == "Mar de los Sargazos" || RT2.text == "Sargazos" || RT2.text == "mar de los sargazos" || RT2.text == "sargazos")
            {
                RT2.textComponent.color = Color.green;
                Debug.Log("BIEN");
            }
            else
            {
                RT2.textComponent.color = Color.red;
            }

            if (RT3.text == "Danubio" || RT3.text == "El Danubio" || RT3.text == "Rio Danubio")
            {
                RT3.textComponent.color = Color.green;
                Debug.Log("BIEN");
            }
            else
            {
                RT3.textComponent.color = Color.red;
            }

            if (RT4.text == "desierto Polar �rtico"  || RT4.text == "Polar �rtico" || RT4.text == "El �rtico" || RT4.text == "Polar Artico")
            {
                RT4.textComponent.color = Color.green;
                Debug.Log("BIEN");
            }
            else
            {
                RT4.textComponent.color = Color.red;
            }
        }


        if (SceneManager.GetSceneByName("LVL2Examen") == SceneManager.GetActiveScene())
        {
            if (RT1.text == "2")
            {
                RT1.textComponent.color = Color.green;
                Debug.Log("BIEN");
            }
            else
            {
                RT1.textComponent.color = Color.red;
            }

            if (RT2.text == "27346")
            {
                RT2.textComponent.color = Color.green;
                Debug.Log("BIEN");
            }
            else
            {
                RT2.textComponent.color = Color.red;
            }

            if (RT3.text == "79380")
            {
                RT3.textComponent.color = Color.green;
                Debug.Log("BIEN");
            }
            else
            {
                RT3.textComponent.color = Color.red;
            }

            if (RT4.text == "-100")
            {
                RT4.textComponent.color = Color.green;
                Debug.Log("BIEN");
            }
            else
            {
                RT4.textComponent.color = Color.red;
            }
        }

       
    }
}
