using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RayCastController : MonoBehaviour
{
    public GameObject profesora;
    public int rayDistance;
    public static bool Perdiste;

    public Transform camara;

    public Transform CamaraPlayer;

    //public static bool vistaAlfrente;

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        DetectarMirando();
    }


    public void DetectarMirando()
    {
        Debug.DrawRay(profesora.transform.position, profesora.transform.TransformDirection(new Vector3(1, 0, 0)) * rayDistance, Color.red);
        RaycastHit hit;


        if (Physics.Raycast(profesora.transform.position, profesora.transform.TransformDirection(new Vector3(1, 0, 0)), out hit, rayDistance))
        {
            //ACA ESCRIBO LOS COMPORTAMIENTOS QUE TENGA SEGUN CON CADA OBJETO
            Debug.Log(hit.transform.tag);

            if (!ChatController.guardado && hit.transform.tag == "Player" )
            {
                Debug.Log("Perdiste");
                Perdiste = true;

            }

            if (!playerController.vistaAlfrente && hit.transform.tag == "Player")
            {
                Debug.Log("Perdiste");
                Perdiste = true;
            }

        }
        //else
        //{
        //    Debug.Log("tranca");
        //}

    }
}
