using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using System;
using System.IO;
using System.Text;
using UnityEngine.Networking;


public class ChatManager : MonoBehaviour
{
    public static Action OnMessageRecived;
    [SerializeField] Mensaje messagePrefab;
    [SerializeField] TMP_InputField inputField;
    [SerializeField] Transform content;
    [SerializeField] GameObject PanelLoading;


    [SerializeField] string APIKey;

    //key:  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
    //key2: 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


    //key3 otra cuenta: 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


    void Start()
    {
        APIKey = Menu.ApiKeyCode;
        //CreateMessage("Porque crees que estas aqui?", false);
        PanelLoading.SetActive(false);
    }

    void CreateMessage(string Text, bool IsUser)
    {
        Mensaje message = Instantiate(messagePrefab, content);

        message.Configure(Text, IsUser);
        PanelLoading.SetActive(false);

        OnMessageRecived.Invoke();

    }
    //IEnumerator FalseChatGpt()
    //{
    //    PanelLoading.SetActive(true);
    //    yield return new WaitForSeconds(2f);
    //    CreateMessage("Mensaje de chat gpt...", false);
    //}
    void SendRequest(string prompt)
    {
        PanelLoading.SetActive(true);
        RequestChatGPT requestChatGPT = new RequestChatGPT();
        requestChatGPT.model = "text-davinci-003";
        requestChatGPT.prompt = prompt;
        requestChatGPT.max_tokens = 1000;
        requestChatGPT.temperature = 0;



        string jsonData = JsonUtility.ToJson(requestChatGPT);
        byte[] data = Encoding.UTF8.GetBytes(jsonData);

        StartCoroutine(SendRequestAPI(data));
    }
    IEnumerator SendRequestAPI(byte[] data)
    {

        UnityWebRequest www = new UnityWebRequest("https://api.openai.com/v1/completions", "POST");
        www.uploadHandler = new UploadHandlerRaw(data);
        www.downloadHandler = new DownloadHandlerBuffer();

        www.SetRequestHeader("Content-Type", "application/json");
        www.SetRequestHeader("Authorization", "Bearer " + APIKey);

        yield return www.SendWebRequest();

        if (www.result == UnityWebRequest.Result.Success)
        {
            //respuesta
            ResponseChatGPT responseChatGPT = JsonUtility.FromJson<ResponseChatGPT>(www.downloadHandler.text);

            CreateMessage(responseChatGPT.choices[0].text, false);
        }
        else
        {
            CreateMessage("Error", false);
        }
        www.Dispose();

    }

    public void Update()
    {
        Enviar();
    }


    public void Enviar()
    {
        if (Input.GetKeyDown(KeyCode.KeypadEnter))
        {
            CreateMessage(inputField.text, true);
            //StartCoroutine(FalseChatGpt());
            SendRequest(inputField.text);
            inputField.text = "";
        }
      
    }


  

}
[Serializable]
public class RequestChatGPT
{
    public string model;
    public string prompt;
    public int max_tokens;
    public int temperature;

}
[Serializable]
public class ResponseChatGPT
{
    public string id;
    public string @object;
    public int created;
    public string model;
    public List<Choice> choices;
    public Usage usage;

    [Serializable]
    public class Choice
    {
        public string text;
        public int index;
        public object logprobs;
        public string finish_reason;
    }
    [Serializable]
    public class Usage
    {
        public int prompt_tokens;
        public int completion_tokens;
        public int total_tokens;

    }
}
