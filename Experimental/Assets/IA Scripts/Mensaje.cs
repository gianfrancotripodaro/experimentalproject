using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class Mensaje : MonoBehaviour
{

    [SerializeField] TextMeshProUGUI messageText;

    [SerializeField] Image background;

    [SerializeField] Color userColor;

    [SerializeField] Color gptColor;

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    public void Configure(string message, bool IsUser)
    {
        background.color = IsUser ? userColor : gptColor;
        messageText.text = message;
    }
}
