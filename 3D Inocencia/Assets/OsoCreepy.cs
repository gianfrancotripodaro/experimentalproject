using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class OsoCreepy : MonoBehaviour
{
    public GameObject Luz;
    public  static bool osoTocar;

    public GameObject panelOso;

    public static int pasoconver;

    public static bool terminoConver;

    public float contPasarEscena;

    public GameObject musica;

    void Start()
    {
        Luz.SetActive(false);
        pasoconver = 0;
    }

    // Update is called once per frame
    void Update()
    {

        if (terminoConver)
        {
            contPasarEscena += Time.deltaTime;
            if (contPasarEscena > 3)
            {
                SceneManager.LoadScene(4);
            }
        }


        if (BebeController.tocarBebe)
        {
            Luz.SetActive(true);
            musica.SetActive(false);
            //osoTocar = true;
        }
        Debug.Log(osoTocar);
        Debug.Log(pasoconver);
    }

    private void OnMouseDown()
    {
        
        panelOso.SetActive(true);
    }

}
