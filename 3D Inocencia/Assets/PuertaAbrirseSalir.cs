using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PuertaAbrirseSalir : MonoBehaviour
{
    public bool puedeAbrir;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
     
    }

    private void OnMouseDown()
    {
        if (puedeAbrir)
        {


            if (SceneManager.GetSceneByName("Park") == SceneManager.GetActiveScene())
            {
                SceneManager.LoadScene(3);
            }
            else
            {
                if (Osito2Controller.tocoOsito)
                {
                    SceneManager.LoadScene(2);
                }
            }
        }
       
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            puedeAbrir = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            puedeAbrir = false;
        }
    }
}
