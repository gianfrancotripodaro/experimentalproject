using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovPersonaje : MonoBehaviour
{
    private Rigidbody rb;
    public float movementSpeed;

    public AudioSource pasos;

    public bool Hactivo;
    public bool Vactivo;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        if (!VentilacionScript.isDialogocreppy && !BarrileteScript.toco)
        {
            float hor = Input.GetAxisRaw("Horizontal");
            float ver = Input.GetAxisRaw("Vertical");

            if (hor != 0 || ver != 0)
            {
                Vector3 direction = (transform.forward * ver + transform.right * hor).normalized;
                rb.velocity = direction * movementSpeed;
            }
            else
            {
                rb.velocity = Vector3.zero;
            }
        }

        if (Input.GetButtonDown("Horizontal"))
        {
            Hactivo = true;
            pasos.Play();
        }
        if (Input.GetButtonDown("Vertical"))
        {
            Vactivo = true;
            pasos.Play();
        }

        if (Input.GetButtonUp("Horizontal"))
        {
            Hactivo = false;
            if (!Vactivo)
            {
                pasos.Pause();
            }
            
        }
        if (Input.GetButtonUp("Vertical"))
        {
            Vactivo = false;
            if (!Hactivo)
            {
                pasos.Pause();
            }
           
        }

    }
}
