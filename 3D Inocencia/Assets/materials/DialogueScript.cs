using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class DialogueScript : MonoBehaviour
{
    // Start is called before the first frame update
    public TextMeshProUGUI dialogueText;

    public string[] lines;

    public float textSpeed;

    public GameObject Marcotxt;

   public  static int index;

    public GameObject NenaPnael;

    public GameObject OsoPnael;

    public GameObject parpados;

    //public GameObject CamaraTe;

    //public GameObject Player;

    //public GameObject nuevoTexto;

    public GameObject Barrilete;

    public static bool agarroBarrilete;
    public static bool escuchoVentilacion;

    public AudioSource audioSource;
    public AudioClip NpcVoice;
    public AudioClip PlayerVoice;
    public AudioClip bebeVoice;

    public int charsToPlay;

    public bool isPlayerTalking;
    public bool bebeTalking;

   //public float tipyingTime;

    void Start()
    {
        //audioSource = GetComponent<AudioSource>();
      


        parpados.SetActive(false);
        dialogueText.text = string.Empty;
        StartDialogue();
    }

    // Update is called once per frame
    void Update()
    {
        if (isPlayerTalking)
        {
            audioSource.clip = PlayerVoice;
        }
        else
        {
           
            if (bebeTalking)
            {
                audioSource.clip = bebeVoice;
            }
            else
            {
                audioSource.clip = NpcVoice;
            }
            
        }
       


        if (SceneManagerScript.TextoPasado == 0 && !VentilacionScript.isDialogocreppy && !BarrileteScript.toco)
        {


            if (index == 0)
            {
                NenaPnael.SetActive(true);
                OsoPnael.SetActive(false);
                isPlayerTalking = true;
            }

            if (index == 1)
            {
                NenaPnael.SetActive(false);
                OsoPnael.SetActive(true);
                isPlayerTalking = false;
            }

            if (index == 2)
            {
                NenaPnael.SetActive(false);
                OsoPnael.SetActive(true);
                isPlayerTalking = false;
            }

            if (index == 3)
            {
                NenaPnael.SetActive(true);
                OsoPnael.SetActive(false);
                isPlayerTalking = true;
            }

            if (index == 4)
            {
                NenaPnael.SetActive(false);
                OsoPnael.SetActive(true);
                isPlayerTalking = false;
            }

            if (index == 5)
            {
                NenaPnael.SetActive(true);
                OsoPnael.SetActive(false);
                isPlayerTalking = true;
            }

        }
        else
        {
            if (SceneManagerScript.TextoPasado > 1 && SceneManagerScript.TextoPasado < 4 /*&& !VentilacionScript.isDialogocreppy && !BarrileteScript.toco*/)
            {
                if (index == 0)
                {
                    NenaPnael.SetActive(false);
                    OsoPnael.SetActive(true);
                    isPlayerTalking = false;
                }

                if (index == 1)
                {
                    NenaPnael.SetActive(true);
                    OsoPnael.SetActive(false);
                    isPlayerTalking = true;
                }

                if (index == 2)
                {
                    NenaPnael.SetActive(false);
                    OsoPnael.SetActive(true);
                    isPlayerTalking = false;
                }

                if (index == 3)
                {
                    NenaPnael.SetActive(true);
                    OsoPnael.SetActive(false);
                    isPlayerTalking = true;
                }

                if (index == 4)
                {
                    NenaPnael.SetActive(true);
                    OsoPnael.SetActive(false);
                    isPlayerTalking = true;
                }

                if (index == 5)
                {
                    NenaPnael.SetActive(false);
                    OsoPnael.SetActive(true);
                    isPlayerTalking = false;
                }

                if (index == 6)
                {
                    NenaPnael.SetActive(false);
                    OsoPnael.SetActive(true);
                    isPlayerTalking = false;
                }

                if (index == 7)
                {
                    NenaPnael.SetActive(false);
                    OsoPnael.SetActive(true);
                    isPlayerTalking = false;
                }

                if (index == 8)
                {
                    NenaPnael.SetActive(true);
                    OsoPnael.SetActive(false);

                    //activar parpadeo
                    //activar player jugable

                    parpados.SetActive(true);
                    //CamaraTe.SetActive(false);
                    isPlayerTalking = true;
                }



                //if (index == 9)
                //{
                //    NenaPnael.SetActive(true);
                //    OsoPnael.SetActive(false);

                //}

            }
           

        }



        if (VentilacionScript.isDialogocreppy)
        {
           
            if (index == 0)
            {
                NenaPnael.SetActive(false);
                OsoPnael.SetActive(false);
                isPlayerTalking = false;
                bebeTalking = true;
            }

            if (index == 1)
            {
                NenaPnael.SetActive(false);
                OsoPnael.SetActive(false);
                bebeTalking = true;
                isPlayerTalking = false;
            }

            if (index == 2)
            {
                NenaPnael.SetActive(false);
                OsoPnael.SetActive(false);
                bebeTalking = true;
                isPlayerTalking = false;
                escuchoVentilacion = true;
            }
        }
        else
        {
            if (BarrileteScript.toco)
            {
             
                if (index == 0)
                {
                    NenaPnael.SetActive(true);
                    OsoPnael.SetActive(false);
                    isPlayerTalking = true;

                    agarroBarrilete = true;
                }

                if (index == 1)
                {
                    NenaPnael.SetActive(true);
                    OsoPnael.SetActive(false);
                    isPlayerTalking = true;
                    BarrileteScript.toco = false;
                    Barrilete.SetActive(false);

                  
                }
            }
        }

        if (Osito2Controller.tocoOsito)
        {
            Debug.Log("1");
            if (index == 0)
            {
                NenaPnael.SetActive(true);
                OsoPnael.SetActive(false);
                Debug.Log("entra primer dialogo");
                //if (index==0)
                //{
                //    index++;
                //}
                isPlayerTalking = true;
            }
            else if (index == 1)
            {
                NenaPnael.SetActive(false);
                OsoPnael.SetActive(true);
                Debug.Log("entra segundo dialogo");
                isPlayerTalking = false;
            }
            else if (index == 2)
            {
                NenaPnael.SetActive(true);
                OsoPnael.SetActive(false);
                Debug.Log("entra tercero dialogo");
                isPlayerTalking = true;
            }
            else if (index == 3)
            {
                NenaPnael.SetActive(true);
                OsoPnael.SetActive(false);
                isPlayerTalking = true;
            }

        }


        if (Input.GetMouseButtonDown(0))
        {
            if (dialogueText.text==lines[index])
            {
                NextLine();
            }
            else
            {
                StopAllCoroutines();
                dialogueText.text = lines[index];
                //VentilacionScript.isDialogocreppy = false;
            }
        }
    }


    public void StartDialogue()
    {
        index = 0;

        StartCoroutine(WriteLine());
    }

    IEnumerator WriteLine()
    {
        int charIndex = 0;

        foreach (char Letter in lines[index].ToCharArray())
        {
            dialogueText.text += Letter;

            if (charIndex % charsToPlay ==0)
            {
                audioSource.Play();
            }

            charIndex++;
            yield return new WaitForSeconds(textSpeed);

        }
    }


    public void NextLine()
    {

        //SceneManagerScript.TextoLeido = 5;

        if (index < lines.Length -1)
        {
            SceneManagerScript.TextoLeido++;
            index++;
            dialogueText.text = string.Empty;
            StartCoroutine(WriteLine());
           
            //aca hacer que cambie entre el oso y la nena y queda goooooooood
        }
        else
        {
            Marcotxt.SetActive(false);
            //index = 0;

          SceneManagerScript.TextoPasado++;


        }
    }

}
