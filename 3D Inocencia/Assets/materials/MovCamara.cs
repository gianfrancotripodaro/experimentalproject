using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovCamara : MonoBehaviour
{
    public Camera fpsCamera;

    public float speedH;
    public float speedV;

    float h;
    float v;

    private void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
    }

    private void Update()
    {
        if (!VentilacionScript.isDialogocreppy && !BarrileteScript.toco)
        {
            h = speedH * Input.GetAxis("Mouse X");
            v = speedV * Input.GetAxis("Mouse Y");

            transform.Rotate(0, h, 0);

            fpsCamera.transform.Rotate(-v, 0, 0);
        }
            
      
    }



}
