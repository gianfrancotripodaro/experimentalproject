using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class DialogueScriptPark : MonoBehaviour
{

    public TextMeshProUGUI dialogueText;

    public string[] lines;

    public float textSpeed;

    int index;


    public GameObject NenaPnael;

    public GameObject OsoPnael;

    public GameObject chica1Panel;

    public GameObject chica1PanelRiendo;

    public GameObject chica2Panel;

    public int PasoConver;


    public AudioSource audioSource;
    public AudioClip NpcVoice;
    public AudioClip PlayerVoice;
    public AudioClip chicasVoice;

    public int charsToPlay;

    public bool isPlayerTalking;
    public bool ChichasTalking;



    void Start()
    {
        dialogueText.text = string.Empty;
        PasoConver = 0;
        StartDialogue();
    }

    // Update is called once per frame
    void Update()
    {

        if (isPlayerTalking)
        {
            audioSource.clip = PlayerVoice;
        }
        else
        {

            if (ChichasTalking)
            {
                audioSource.clip = chicasVoice;
            }
            else
            {
                audioSource.clip = NpcVoice;
            }

        }

        if (Input.GetMouseButtonDown(0))
        {
            if (dialogueText.text == lines[index])
            {
                NextLine();
            }
            else
            {
                StopAllCoroutines();
                dialogueText.text = lines[index];
            }
        }


        if (osoParkController.OsoToco && MuņecaController.MuņecasCharlas)
        {
            if (index == 0)
            {
                NenaPnael.SetActive(true);
                OsoPnael.SetActive(false);
                isPlayerTalking = true;

            }
            if (index == 1)
            {
                NenaPnael.SetActive(true);
                OsoPnael.SetActive(false);
                isPlayerTalking = true;

            }
            if (index == 2)
            {
                NenaPnael.SetActive(true);
                OsoPnael.SetActive(false);
                isPlayerTalking = true;

            }
            if (index == 3)
            {
                NenaPnael.SetActive(false);
                OsoPnael.SetActive(true);
                isPlayerTalking = false;

            }
            if (index == 4)
            {
                NenaPnael.SetActive(false);
                OsoPnael.SetActive(true);
                isPlayerTalking = false;

            }
            if (index == 5)
            {
                NenaPnael.SetActive(true);
                OsoPnael.SetActive(false);
                isPlayerTalking = true;
            }
        }


        if (/*Osito2Controller.tocoOsito && */!MuņecaController.MuņecasCharlas && PasoConver == 0)
        {
            if (index == 0)
            {
                NenaPnael.SetActive(true);
                OsoPnael.SetActive(false);
                isPlayerTalking = true;
            }
            if (index == 1)
            {
                NenaPnael.SetActive(true);
                OsoPnael.SetActive(false);
                isPlayerTalking = true;

            }
            if (index == 2)
            {
                NenaPnael.SetActive(true);
                OsoPnael.SetActive(false);
                isPlayerTalking = true;
            }
            if (index == 3)
            {
                NenaPnael.SetActive(true);
                OsoPnael.SetActive(false);
                isPlayerTalking = true;

            }
            if (index == 4)
            {
                NenaPnael.SetActive(true);
                OsoPnael.SetActive(false);
                isPlayerTalking = true;

            }
            if (index == 5)
            {
                NenaPnael.SetActive(false);
                OsoPnael.SetActive(true);
                isPlayerTalking = false;

            }
            if (index == 6)
            {
                NenaPnael.SetActive(false);
                OsoPnael.SetActive(true);
                isPlayerTalking = false;
            }
            if (index == 7)
            {
                NenaPnael.SetActive(true);
                OsoPnael.SetActive(false);
                isPlayerTalking = true;

            }
            if (index == 8)
            {
                NenaPnael.SetActive(true);
                OsoPnael.SetActive(false);
                isPlayerTalking = true;

            }
            if (index == 9)
            {
                NenaPnael.SetActive(true);
                OsoPnael.SetActive(false);
                isPlayerTalking = true;

            }
            if (index == 10)
            {
                NenaPnael.SetActive(true);
                OsoPnael.SetActive(false);
                isPlayerTalking = true;

            }
            if (index == 11)
            {
                NenaPnael.SetActive(false);
                OsoPnael.SetActive(true);
                isPlayerTalking = false;

            }
            if (index == 12)
            {
                NenaPnael.SetActive(false);
                OsoPnael.SetActive(true);
                isPlayerTalking = false;


            }
            if (index == 13)
            {
                NenaPnael.SetActive(true);
                OsoPnael.SetActive(false);
                isPlayerTalking = true;
                PasoConver++;
                osoParkController.OsoToco = true;
            }
            if (index == 14)
            {
                NenaPnael.SetActive(true);
                OsoPnael.SetActive(false);
                isPlayerTalking = true;
                PasoConver++;
                osoParkController.OsoToco = true;
            }
                //}
                ////if (index == 15)
                ////{
                ////    //NenaPnael.SetActive(true);
                ////    //OsoPnael.SetActive(false);

                ////}
            }
            else
        {
            
            
                if (index == 0)
                {
                    NenaPnael.SetActive(true);
                    OsoPnael.SetActive(false);
                    chica1Panel.SetActive(false);
                    chica1PanelRiendo.SetActive(false);
                    chica2Panel.SetActive(false);
                     isPlayerTalking = true;
            }
                if (index == 1)
                {
                    NenaPnael.SetActive(true);
                    OsoPnael.SetActive(false);
                    chica1Panel.SetActive(false);
                    chica1PanelRiendo.SetActive(false);
                    chica2Panel.SetActive(false);
                    isPlayerTalking = true;

            }
                if (index == 2)
                {
                    NenaPnael.SetActive(true);
                    OsoPnael.SetActive(false);
                    chica1Panel.SetActive(false);
                    chica1PanelRiendo.SetActive(false);
                    chica2Panel.SetActive(false);
                    isPlayerTalking = true;
            }
                if (index == 3)
                {
                    NenaPnael.SetActive(false);
                    OsoPnael.SetActive(false);
                    chica1Panel.SetActive(true);
                    chica1PanelRiendo.SetActive(false);
                    chica2Panel.SetActive(false);
                isPlayerTalking = false;
                ChichasTalking = true;

            }
                if (index == 4)
                {
                    NenaPnael.SetActive(false);
                    OsoPnael.SetActive(false);
                    chica1Panel.SetActive(true);
                    chica1PanelRiendo.SetActive(false);
                    chica2Panel.SetActive(false);
                isPlayerTalking = false;
                ChichasTalking = true;
            }
                if (index == 5)
                {
                    NenaPnael.SetActive(false);
                    OsoPnael.SetActive(false);
                    chica1Panel.SetActive(false);
                    chica1PanelRiendo.SetActive(true);
                    chica2Panel.SetActive(false);
                isPlayerTalking = false;
                ChichasTalking = true;
            }
                if (index == 6)
                {
                    NenaPnael.SetActive(false);
                    OsoPnael.SetActive(false);
                    chica1Panel.SetActive(true);
                    chica1PanelRiendo.SetActive(false);
                    chica2Panel.SetActive(false);
                isPlayerTalking = false;
                ChichasTalking = true;

            }
                if (index == 7)
                {
                    NenaPnael.SetActive(false);
                    OsoPnael.SetActive(false);
                    chica1Panel.SetActive(false);
                    chica1PanelRiendo.SetActive(true);
                    chica2Panel.SetActive(false);
                isPlayerTalking = false;
                ChichasTalking = true;

            }
                if (index == 8)
                {

                    NenaPnael.SetActive(false);
                    OsoPnael.SetActive(false);
                    chica1Panel.SetActive(false);
                    chica1PanelRiendo.SetActive(false);
                    chica2Panel.SetActive(true);
                isPlayerTalking = false;
                ChichasTalking = true;
            }
                if (index == 9)
                {
                    NenaPnael.SetActive(true);
                    OsoPnael.SetActive(false);
                    chica1Panel.SetActive(false);
                    chica1PanelRiendo.SetActive(false);
                    chica2Panel.SetActive(false);
                isPlayerTalking = true;
                ChichasTalking = false;

            }
            
        }

       

    }

    

    public void StartDialogue()
    {
        index = 0;

        StartCoroutine(WriteLine());
    }

    IEnumerator WriteLine()
    {
        int charIndex = 0;


        foreach (char Letter in lines[index].ToCharArray())
        {
            dialogueText.text += Letter;

            if (charIndex % charsToPlay == 0)
            {
                audioSource.Play();
            }
            charIndex++;
            yield return new WaitForSeconds(textSpeed);

        }
    }


    public void NextLine()
    {
        if (index < lines.Length - 1)
        {
            index++;
            dialogueText.text = string.Empty;
            StartCoroutine(WriteLine());
        }
        else
        {
            gameObject.SetActive(false);
        }
    }

}
