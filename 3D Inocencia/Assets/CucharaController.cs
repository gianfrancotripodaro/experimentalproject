using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CucharaController : MonoBehaviour
{
    public GameObject cucharaSin;
    public GameObject cucharaConAzucar;
     Rigidbody cucharaRB;
    public bool Cuchara;
    public bool CucharaCon;

    public static int Cont;
    void Start()
    {
        cucharaRB = GetComponent<Rigidbody>();
        //cucharaSin = GetComponent<Rigidbody>();
        Cuchara = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (Cuchara)
        {
            cucharaSin.SetActive(true);
            cucharaConAzucar.SetActive(false);
            //sonido agarrar azucar
        }


        if (CucharaCon)
        {
            cucharaConAzucar.SetActive(true);
            cucharaSin.SetActive(false);
            //sonido dejar azucar
        }
    }


    private void OnMouseDrag()
    {
        //if (Cuchara)
        //{
        //    cucharaSin
        //}
        //else
        //{

        //}
        //if (gameObject.name == "Jorge")
        //{

        //}
        Vector3 mousePosition = new Vector3(Input.mousePosition.x, Input.mousePosition.y, -Camera.main.transform.position.z /*+ transform.position.z*/ + -8.3F);
        Vector3 objPosition = Camera.main.ScreenToWorldPoint(mousePosition);
        transform.position = objPosition;
        cucharaRB.isKinematic = true;

        Cuchara = true;

        //Debug.Log("AGARRASNDO " + transform.position.z);

    }

    private void OnMouseUp()
    {
        cucharaRB.isKinematic = false;
        Cuchara = false;
    }


    //private void OntriggerEnter(Collision collision)
    //{

    //}


    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Azucar" && CucharaCon)
        {
            CucharaCon = false;
            //sonido agarrar azucar
        }

        if (other.gameObject.tag == "Azucar" && !CucharaCon)
        {
            CucharaCon = true;
            //sonido dejar azucar
        }


        if (other.gameObject.tag == "Te" && CucharaCon)
        {
            CucharaCon = false;

            Cont++;
        }


    }


}
