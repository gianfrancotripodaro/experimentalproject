using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;
public class DialogoCreepy : MonoBehaviour
{
    public TextMeshProUGUI dialogueText;

    public string[] lines;

    public float textSpeed;

    int index;



    public GameObject OsoPanel;

    public GameObject NenaPanel;

    public GameObject bebePanel;

    public bool cont;

    public float contSalir;

    public bool terminoDial;

    public AudioSource audioSource;
    public AudioClip NpcVoice;
    public AudioClip PlayerVoice;
    public AudioClip bebeVoice;

    public int charsToPlay;

    public bool isPlayerTalking;
    public bool bebeTalking;

    public GameObject panelFinal;
    void Start()
    {
        dialogueText.text = string.Empty;
        StartDialogue();
    }

    // Update is called once per frame
    void Update()
    {

        if (isPlayerTalking)
        {
            audioSource.clip = PlayerVoice;
        }
        else
        {

            if (bebeTalking)
            {
                audioSource.clip = bebeVoice;
            }
            else
            {
                audioSource.clip = NpcVoice;
            }

        }


        if (Input.GetMouseButtonDown(0))
        {
            if (dialogueText.text == lines[index])
            {
                NextLine();
            }
            else
            {
                StopAllCoroutines();
                dialogueText.text = lines[index];
            }
        }


       

        if (terminoDial)
        {
            contSalir += Time.deltaTime;
            if (contSalir>3)
            {
                //Application.Quit();
                //Debug.Log("Salio");

                panelFinal.SetActive(true);

                if (Input.GetMouseButtonDown(0))
                {
                    Debug.Log("Salio");
                    Application.Quit();
                }
            }
        

        }


        if (SceneManager.GetSceneByName("TeFinal") == SceneManager.GetActiveScene())
        {
            if (index == 0)
            {
                NenaPanel.SetActive(false);
                OsoPanel.SetActive(true);
                bebePanel.SetActive(false);
                isPlayerTalking = false;

            }
            if (index == 1)
            {
                NenaPanel.SetActive(true);
                OsoPanel.SetActive(false);
                bebePanel.SetActive(false);
                isPlayerTalking = true;

            }
            if (index == 2)
            {
                NenaPanel.SetActive(true);
                OsoPanel.SetActive(false);
                bebePanel.SetActive(false);
                isPlayerTalking = true;

            }
            if (index == 3)
            {
                NenaPanel.SetActive(true);
                OsoPanel.SetActive(false);
                bebePanel.SetActive(false);
                isPlayerTalking = true;

            }

            if (index == 4)
            {
                NenaPanel.SetActive(true);
                OsoPanel.SetActive(false);
                bebePanel.SetActive(false);
                isPlayerTalking = true;

            }
            if (index == 5)
            {
                NenaPanel.SetActive(false);
                OsoPanel.SetActive(true);
                bebePanel.SetActive(false);
                isPlayerTalking = false;

            }

            if (index == 6)
            {
                NenaPanel.SetActive(false);
                OsoPanel.SetActive(true);
                bebePanel.SetActive(false);
                isPlayerTalking = false;
                terminoDial = true;
              

            }

        }
        else
        {
            if (index == 0)
            {
                NenaPanel.SetActive(true);
                OsoPanel.SetActive(false);
                bebePanel.SetActive(false);
                isPlayerTalking = true;

            }
            if (index == 1)
            {
                NenaPanel.SetActive(true);
                OsoPanel.SetActive(false);
                bebePanel.SetActive(false);
                isPlayerTalking = true;

            }
            if (index == 2)
            {
                NenaPanel.SetActive(false);
                OsoPanel.SetActive(false);
                bebePanel.SetActive(true);
                isPlayerTalking = false;
                bebeTalking = true;
            }
            if (index == 3)
            {
                NenaPanel.SetActive(false);
                OsoPanel.SetActive(false);
                bebePanel.SetActive(true);
                isPlayerTalking = false;
                bebeTalking = true;

            }

            if (index == 4)
            {
                NenaPanel.SetActive(false);
                OsoPanel.SetActive(false);
                bebePanel.SetActive(true);

                isPlayerTalking = false;
                bebeTalking = true;
            }
        }
      
            

        



    }



    public void StartDialogue()
    {
        index = 0;

        StartCoroutine(WriteLine());
    }

    IEnumerator WriteLine()
    {
        int charIndex = 0;

        foreach (char Letter in lines[index].ToCharArray())
        {
            dialogueText.text += Letter;

            if (charIndex % charsToPlay == 0)
            {
                audioSource.Play();
            }

            charIndex++;
            yield return new WaitForSeconds(textSpeed);

        }
    }


    public void NextLine()
    {
        if (index < lines.Length - 1)
        {
            index++;
            dialogueText.text = string.Empty;
            StartCoroutine(WriteLine());
        }
        else
        {
            gameObject.SetActive(false);
          
        }
    }

}
