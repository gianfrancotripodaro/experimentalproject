using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class menuController : MonoBehaviour
{


    public GameObject PanelOptions;

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    public void Play()
    {
        SceneManager.LoadScene(1);
    }


    public void Options()
    {
        PanelOptions.SetActive(true);
    }

    public void CerrarOptions()
    {
        PanelOptions.SetActive(false);
    }

}
