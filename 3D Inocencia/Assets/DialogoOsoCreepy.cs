using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.Animations;
using UnityEngine.SceneManagement;

public class DialogoOsoCreepy : MonoBehaviour
{
    public TextMeshProUGUI dialogueText;

    public string[] lines;

    public float textSpeed;

    int index;



    public GameObject OsoPanel;

    public GameObject NenaPanel;


    public float contPasarEscena;



    public AudioSource audioSource;
    public AudioClip NpcVoice;
    public AudioClip PlayerVoice;


    public int charsToPlay;

    public bool isPlayerTalking;




    void Start()
    {
        dialogueText.text = string.Empty;
        StartDialogue();
    }

    void Update()
    {

        if (isPlayerTalking)
        {
            audioSource.clip = PlayerVoice;
        }
        else
        {
                audioSource.clip = NpcVoice;
        }

        if (Input.GetMouseButtonDown(0))
        {
            if (dialogueText.text == lines[index])
            {
                NextLine();
            }
            else
            {
                StopAllCoroutines();
                dialogueText.text = lines[index];
            }
        }



     

            if (index == 0)
            {
                OsoPanel.SetActive(true);
                NenaPanel.SetActive(false);
                 isPlayerTalking = false;
            }
            if (index == 1)
            {
                NenaPanel.SetActive(true);
                OsoPanel.SetActive(false);
            isPlayerTalking = true;


        }
            if (index == 2)
            {
                NenaPanel.SetActive(true);
                OsoPanel.SetActive(false);
            isPlayerTalking = true;


        }
            if (index == 3)
            {
                NenaPanel.SetActive(false);
                OsoPanel.SetActive(true);
            isPlayerTalking = false;


        }

            if (index == 4)
            {
                NenaPanel.SetActive(false);
                OsoPanel.SetActive(true);
            isPlayerTalking = false;

        }


        if (index == 5)
        {
            NenaPanel.SetActive(false);
            OsoPanel.SetActive(true);
            isPlayerTalking = false;

        }

        if (index == 6)
        {
            NenaPanel.SetActive(false);
            OsoPanel.SetActive(true);
            isPlayerTalking = false;

        }

        if (index == 7)
        {
            NenaPanel.SetActive(false);
            OsoPanel.SetActive(true);
            isPlayerTalking = false;

        }

        if (index == 8)
        {
            NenaPanel.SetActive(false);
            OsoPanel.SetActive(true);
            isPlayerTalking = false;

        }

        if (index == 9)
        {
            NenaPanel.SetActive(false);
            OsoPanel.SetActive(true);
            isPlayerTalking = false;

        }

        if (index == 10)
        {
            NenaPanel.SetActive(false);
            OsoPanel.SetActive(true);
            isPlayerTalking = false;

        }

        if (index == 11)
        {
            NenaPanel.SetActive(false);
            OsoPanel.SetActive(true);
            isPlayerTalking = false;

        }

        if (index == 12)
        {
            NenaPanel.SetActive(false);
            OsoPanel.SetActive(true);
            isPlayerTalking = false;

        }

        if (index == 13)
        {
            NenaPanel.SetActive(false);
            OsoPanel.SetActive(true);
            isPlayerTalking = false;

        }

        if (index == 14)
        {
            NenaPanel.SetActive(false);
            OsoPanel.SetActive(true);
            isPlayerTalking = false;

        }

        if (index == 15)
        {
            NenaPanel.SetActive(true);
            OsoPanel.SetActive(false);
            isPlayerTalking = true;

        }

        if (index == 16)
        {
            NenaPanel.SetActive(true);
            OsoPanel.SetActive(false);
            isPlayerTalking = true;

        }

        if (index == 17)
        {
            NenaPanel.SetActive(true);
            OsoPanel.SetActive(false);
            isPlayerTalking = true;

        }

        if (index == 18)
        {
            NenaPanel.SetActive(true);
            OsoPanel.SetActive(false);
            isPlayerTalking = true;

        }

        if (index == 19)
        {
            NenaPanel.SetActive(true);
            OsoPanel.SetActive(false);
            isPlayerTalking = true;

        }

        if (index == 20)
        {
            NenaPanel.SetActive(false);
            OsoPanel.SetActive(true);
            isPlayerTalking = false;

        }


        if (index == 21)
        {
            NenaPanel.SetActive(false);
            OsoPanel.SetActive(true);
            isPlayerTalking = false;
            OsoCreepy.terminoConver = true;
        }

    }

    public void StartDialogue()
    {
        index = 0;

        StartCoroutine(WriteLine());
    }

    IEnumerator WriteLine()
    {
        int charIndex = 0;

        foreach (char Letter in lines[index].ToCharArray())
        {
            dialogueText.text += Letter;

            if (charIndex % charsToPlay == 0)
            {
                audioSource.Play();
            }
            charIndex++;
            yield return new WaitForSeconds(textSpeed);

        }
    }


    public void NextLine()
    {
        if (index < lines.Length - 1)
        {
            index++;
            dialogueText.text = string.Empty;
            StartCoroutine(WriteLine());
        }
        else
        {
            gameObject.SetActive(false);

        }
    }

}
